#include "Graph.hpp"

Graph_t::Graph_t(int nb_vertex)
{
    this->nb_vertex = nb_vertex;
    this->adjacents = new list<int>[nb_vertex];
}

void Graph_t::DFS(int nb_vertex, bool visited_vertex[])
{
    // Mark the current node as visited
    visited_vertex[nb_vertex] = true;
  
    // Recur for all the vertices adjacent to this vertex
    list<int>::iterator i;
    for (i = adjacents[nb_vertex].begin(); i != adjacents[nb_vertex].end(); ++i)
        if (!visited_vertex[*i])
            DFS(*i, visited_vertex);
}
  
Graph_t Graph_t::get_transpose()
{
    Graph_t g(nb_vertex);
    for (int v = 0; v < nb_vertex; v++)
    {
        // Recur for all the vertices adjacent to this vertex
        list<int>::iterator i;
        for(i = adjacents[v].begin(); i != adjacents[v].end(); ++i)
        {
            g.adjacents[*i].push_back(v);
        }
    }
    return g;
}
  
void Graph_t::add_adjacent(int vertex, int adjacent)
{
    this->adjacents[vertex].push_back(adjacent); // Add w to v’s list.
}
  
void Graph_t::fill_stack(int vertex, bool visited_vertex[], stack<int>& stack)
{
    // Mark the current node as visited and print it
    visited_vertex[vertex] = true;
  
    // Recur for all the vertices adjacent to this vertex
    list<int>::iterator i;
    for(i = adjacents[vertex].begin(); i != adjacents[vertex].end(); ++i)
        if(!visited_vertex[*i])
            fill_stack(*i, visited_vertex, stack);
  
    // All vertices reachable from v are processed by now, push v 
    stack.push(vertex);
}

int Graph_t::print_nb_connected_composante()
{
    stack<int> stack;
    int nb_connectd_composante = 0;
  
    // Mark all the vertices as not visited (For first DFS)
    bool *visited_vertex = new bool[nb_vertex];
    for(int i = 0; i < nb_vertex; i++)
        visited_vertex[i] = false;
  
    // Fill vertices in stack according to their finishing times
    for(int i = 0; i < nb_vertex; i++)
        if(visited_vertex[i] == false)
            fill_stack(i, visited_vertex, stack);
  
    // Create a reversed graph
    Graph_t gr = get_transpose();
  
    // Mark all the vertices as not visited (For second DFS)
    for(int i = 0; i < nb_vertex; i++)
        visited_vertex[i] = false;
  
    // Now process all vertices in order defined by Stack
    while (stack.empty() == false)
    {
        // Pop a vertex from stack
        int v = stack.top();
        stack.pop();
  
        // Print Strongly connected component of the popped vertex
        if (visited_vertex[v] == false)
        {
            gr.DFS(v, visited_vertex);
            cout << endl;
            nb_connectd_composante++;
        }
        
    }
    return nb_connectd_composante;
}
  