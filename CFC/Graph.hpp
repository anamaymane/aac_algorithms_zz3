#include <iostream>
#include <list>
#include <stack>

using namespace std;


/*******************************************************************************************
* @brief Graph_t : this class represente the structure of a graph
* we found number of vertex and the adjacents of each vertex
*******************************************************************************************/
class Graph_t
{
    int         nb_vertex;    // Number of vertex in a graph
    list<int>   *adjacents;    // An array of adjacency lists
  
  public:

    /******************************************************************************************
     * @brief Graph_t : the constructor
     * 
     * input    : number of vertex of a graph
     * output   : a Graph_t object
     ******************************************************************************************/
    Graph_t(int nb_vertex);

    /******************************************************************************************
     * @brief fill_stack : fill a stack with visited vertex,
     * it'a a recusrsive function
     * 
     * input : 
     *      vertex          : a vertex, 
     *      visited_vertex  : an array to check if a element
     *                        is visited or not.
     *      stack           : a stack of visited vertexs
     *
     * output: void
     *
     ******************************************************************************************/
    void fill_stack(int vertex, bool visited_vertex[], stack<int>& stack);
  
    /******************************************************************************************
     * @brief DFS : epth-First-Search, * it'a a recusrsive 
     * function
     * 
     * input : 
     *      vertex          : a vertex, 
     *      visited_vertex  : an array to check if a element
     *                        is visited or not.
     *
     * output: void
     *
     ******************************************************************************************/
    void DFS(int nb_vertex, bool visited_vertex[]);


    /******************************************************************************************
     * @brief add_adjacent : added adjacents to the matrix of adjacents for each vertex
     * 
     * input : 
     *      vertex          : a vertex, 
     *      visited_vertex  : an array to check if a vertex
     *                        is visited or not.
     *
     * output: void
     *
     ******************************************************************************************/
    void add_adjacent(int vertex, int adjacent);
  
    /******************************************************************************************
     * @brief print_nb_connected_composante : The main function that finds and prints strongly 
     * connected composants
     * 
     * input : void
     *
     * output: number of strongly connected composants
     *
     ******************************************************************************************/
    int print_nb_connected_composante();
    
    /*******************************************************************************************
     * @brief get_transpose : Function that returns reverse (or transpose) of this graph
     * 
     * input : void
     *
     * output: Graph_t
     *
     ******************************************************************************************/
    Graph_t get_transpose();
};
  

