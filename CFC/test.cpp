
#include "Graph.hpp"
#include "catch.hpp"



TEST_CASE("Test 1")
{

    int nb_connected_composante = 3;
    int nb_vertex = 5;
    Graph_t graph(nb_vertex);
    graph.add_adjacent(0, 2);
    graph.add_adjacent(0, 3);
    graph.add_adjacent(1, 0);
    graph.add_adjacent(2, 1);
    graph.add_adjacent(3, 4);
  

    REQUIRE(graph.print_nb_connected_composante() == nb_connected_composante);
}

TEST_CASE("Test 2")
{
 
    int nb_connected_composante = 4;
    int nb_vertex = 6;
    Graph_t graph(nb_vertex);

    graph.add_adjacent(0, 1);
    graph.add_adjacent(0, 2);
    graph.add_adjacent(1, 2);
    graph.add_adjacent(1, 5);
    graph.add_adjacent(2, 3);
    graph.add_adjacent(3, 1);
    graph.add_adjacent(3, 4);
    graph.add_adjacent(5, 4);
    
 
    REQUIRE(graph.print_nb_connected_composante() == nb_connected_composante);
}