# AAC_algorithms_ZZ3



## Compile and run projects

### Codage de Huffman
1. **$_> cd huffman_coding**
2. **$_> make test**
3. **./huffman_encoding_test**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;              //Pour lancer les tests
4. **$_> make**
5. **./huffman_encoding**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                   //Pour lancer le programme principale de compression / décompression

### CFC : Composante fortement connexe
1. **$_> cd CFC/build**
2. **$_> cmake ../CMakeLists.txt**
3. **$_> cd ../**
4. **$_> make**
5. **./test**

