/* -------------------------------------------------------------------- */
/* Nom du fichier: tests.cpp                                            */
/* Rôle: Tests unitaires pour tester les différentes fonctions utilisés */
/*       lors de la compression et la décompression avec Huffman        */
/*       et aussi pour éviter la régression de code au cas d'ajout de   */
/*       nouvelles fonctionnalités                                      */
/* -------------------------------------------------------------------- */

#include "library/catch.hpp"
#include "huffman_encoding.hpp"
#include "file_utility.hpp"


TEST_CASE("Testing if huffman generated encoding is correct or not") {
    std::vector<HuffmanNode>        huffmanHeap;
    HuffmanEncodingUtility          huffmanEncodingUtility;
    std::string                     fileContentInHuffmanFormat;
    std::string                     predicted_huffman_output;

    huffmanEncodingUtility.FILE_NAME = "test/small_text_file.txt";
    huffmanEncodingUtility.read_data_file(huffmanHeap);
    huffmanEncodingUtility.construct_huffman_tree(huffmanHeap);
    huffmanEncodingUtility.encode_huffman_tree(huffmanHeap[0]);

    fileContentInHuffmanFormat = huffmanEncodingUtility.get_file_content_in_huffman_format();
    predicted_huffman_output = "010001010110011111101100111111000100011100110100000"
                                           "100110101101100011111011111011110001110101110001101"
                                           "110110010001000111010011001001001";

    REQUIRE( fileContentInHuffmanFormat == predicted_huffman_output);
}

TEST_CASE("Testing file size") {

    std::string                                         FILE_NAME;
    int                                                 originalFileNameSize;
    HuffmanEncodingUtility                              huffmanEncodingUtility;
    int                                                 compressedFileNameSize;

    FILE_NAME  = "test/long_text_file.txt";
    originalFileNameSize = FileUtility::get_file_size(FILE_NAME);

    huffmanEncodingUtility.FILE_NAME = FILE_NAME;
    huffmanEncodingUtility.compress_data();
    huffmanEncodingUtility.FILE_NAME = FILE_NAME + ".compressed";
    huffmanEncodingUtility.uncompress_data(); 

    compressedFileNameSize = FileUtility::get_file_size(FILE_NAME + ".compressed");

    REQUIRE( originalFileNameSize > compressedFileNameSize);
}



TEST_CASE("Testing if huffman uncompression restores the original data (If it is lossless)") {

    std::string                     FILE_NAME;
    FILE *                          file;
    FILE *                          compressedFile;
    bool                            comparaisonResult;
    HuffmanEncodingUtility          huffmanEncodingUtility;

    FILE_NAME  = "test/small_text_file.txt";
    
    huffmanEncodingUtility.FILE_NAME = FILE_NAME;
    huffmanEncodingUtility.compress_data();
    huffmanEncodingUtility.FILE_NAME = FILE_NAME + ".compressed";
    huffmanEncodingUtility.uncompress_data();

    file = fopen((FILE_NAME).c_str(), "rb");
    compressedFile = fopen((huffmanEncodingUtility.FILE_NAME + ".uncompressed.txt").c_str(), "rb");
    comparaisonResult = FileUtility::compareFiles(file, compressedFile);

    REQUIRE( comparaisonResult == true);
}

