/* -------------------------------------------------------------------- */
/* Nom du fichier: main.cpp                                             */
/* Rôle: Programme principale pour compresser et décompresser un fichier*/
/*       , le exe de ce programme principale peut être généré avec      */
/*       la commande make                                               */
/* -------------------------------------------------------------------- */


#include "huffman_encoding.hpp"

int main() {

    HuffmanEncodingUtility              huffmanEncodingUtility;
    int                                 compressOrDecompress;
    std::string                         filePath;

    do {

        std::cout << "Veuillez chosir une opération : " << std::endl;
        std::cout << "\t1-Compresser" << std::endl;
        std::cout << "\t2-Décompresser" << std::endl;
        std::cin >> compressOrDecompress;

        switch(compressOrDecompress) {
            case 1: 
                std::cout << "Veuillez saisir le path du fichier à compresser" << std::endl;
                std::cin >> filePath;
                huffmanEncodingUtility.FILE_NAME = filePath;
                huffmanEncodingUtility.compress_data();
                std::cout << "The compression has succeeded, the compressed file was created"
                             " in the same directory as the original file" << std::endl;
                break;
            case 2: 
                std::cout << "Veuillez saisir le path du fichier à décompresser" << std::endl;
                std::cin >> filePath;
                huffmanEncodingUtility.FILE_NAME = filePath;
                huffmanEncodingUtility.uncompress_data();
                std::cout << "The decompression has succeeded, the original file was created"
                             " in the same directory as the compressed file you've feeded" << std::endl;
                break;
            default: 
                std::cout << "Veuillez choisir une opération valide" << std::endl;
                break;
        }

    } while(compressOrDecompress != 1 && compressOrDecompress != 2);
    
    return 0;

}

//"test/long_text_file.txt"