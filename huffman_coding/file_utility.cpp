/* --------------------------------------------------------------------*/
/* Nom du fichier: file_utility.cpp                                    */
/* Rôle: Fichier source, Implémentation des méthodes du fichier        */
/*       d'entête file_utility.hpp                                     */
/* --------------------------------------------------------------------*/

#include "file_utility.hpp"

int FileUtility::get_file_size(std::string FILE_NAME) {

    std::ifstream       in;
    int                 size;
    
    in.open(FILE_NAME.c_str(), std::ios_base::binary);
    in.seekg(0,std::ios_base::end);
    size = in.tellg();
    in.close();
    return size;
}

bool FileUtility::compareFiles(FILE *file1, FILE *file2) {
    
   char     ch1 = getc(file1);
   char     ch2 =       getc(file2);
   int      pos =       0,
            line =      1;

   while (ch1 != EOF && ch2 != EOF){
      pos++;
      if (ch1 == '\n' && ch2 == '\n'){
         line++;
         pos = 0;
      }
      if (ch1 != ch2){
         return false;
      }
      ch1 = getc(file1);
      ch2 = getc(file2);
   }
   return true;
}
