/* -------------------------------------------------------------------- */
/* Nom du fichier: huffman_encoding.cpp                                 */
/* Rôle: Fichier source, Implémentation des méthodes du fichier         */
/*       d'entête huffman_encoding.hpp                                  */
/* -------------------------------------------------------------------- */

#include "huffman_encoding.hpp"
#include "file_utility.hpp"

void HuffmanEncodingUtility::read_data_file(std::vector<HuffmanNode>& elements) {

    int                 FILE_SIZE = FileUtility::get_file_size(this->FILE_NAME);
    std::ifstream       in;
    char *              buffer;

    using set_iterator = std::vector<HuffmanNode>::iterator;

    in.open(FILE_NAME, std::ios::in | std::ios::binary);
    if (!in.is_open()) {
        std::cerr << "Error in open file '" << FILE_NAME << "'" << std::endl;
        return;
    }

    buffer = new char[FILE_SIZE];
    in.read(buffer, FILE_SIZE);

    for(int i = 0; i < FILE_SIZE; i++) {
        
        if(set_iterator it = std::find(elements.begin(), elements.end(),HuffmanNode(buffer[i], 0));
                    it != elements.end()) {
            it->setFrequency(it->getFrequency() + 1);
        }
        else {
            elements.push_back(HuffmanNode(buffer[i], 1));
        }
    }

    std::sort(elements.begin(), elements.end(), [](HuffmanNode& ef1, HuffmanNode& ef2){
        return ef1.getFrequency() < ef2.getFrequency();
    });
    
    delete buffer;
    in.close();
}

void HuffmanEncodingUtility::construct_huffman_tree(std::vector<HuffmanNode>& huffmanHeap) {

    HuffmanNode*        leftNode;
    HuffmanNode*        rightNode;
    HuffmanNode*        newInternalNode;

    if(huffmanHeap.size() > 1) {
        
        leftNode = new HuffmanNode(huffmanHeap[0]);
        
        rightNode = new HuffmanNode(huffmanHeap[1]);
        
        newInternalNode = new HuffmanNode('$',
                                        huffmanHeap[0].getFrequency() + huffmanHeap[1].getFrequency(),
                                        NodeType::Internal);

        newInternalNode->leftNode = leftNode;
        newInternalNode->rightNode = rightNode;
        huffmanHeap.erase(huffmanHeap.begin(), huffmanHeap.begin() + 2);
        huffmanHeap.push_back(*newInternalNode);
        
        std::sort(huffmanHeap.begin(), huffmanHeap.end(), [](HuffmanNode& ef1, HuffmanNode& ef2){
            return ef1.getFrequency() < ef2.getFrequency();
        });
        
        construct_huffman_tree(huffmanHeap);
    }
}

void HuffmanEncodingUtility::encode_huffman_tree_recursive(HuffmanNode& node, std::string& rep) {

    std::string         bitEncodingLeft(rep);
    std::string         bitEncodingRight(rep);


    if(node.getNodeType() == NodeType::Normal) {
        node.setBitRepresentation(rep);
        characterBitRepresentation.insert(std::pair<char, std::string>(node.getCharacter(), rep));
    }
        
    bitEncodingLeft.push_back('0');
    bitEncodingRight.push_back('1');

    if(node.leftNode != NULL)
        encode_huffman_tree_recursive(*node.leftNode, bitEncodingLeft);
    if(node.rightNode != NULL)
        encode_huffman_tree_recursive(*node.rightNode, bitEncodingRight);
}

void HuffmanEncodingUtility::encode_huffman_tree(HuffmanNode& node) {
    std::string         bitEncodingRoot;
    encode_huffman_tree_recursive(node, bitEncodingRoot);
}

void HuffmanEncodingUtility::display_huffman_tree(HuffmanNode& node) {
    std::cout << node.getCharacter() << "/" << node.getFrequency() << "/" << node.getNodeBitRepresentation() << std::endl;
    if(node.leftNode != NULL) {
        display_huffman_tree(*node.leftNode);
        display_huffman_tree(*node.rightNode);
    }
}

std::string HuffmanEncodingUtility::get_file_content_in_huffman_format() {

    int                     FILE_SIZE;
    std::ifstream           in;
    std::string             fileContentHuffmanRepresentation;
    char *                  buffer;

    using set_iterator = std::vector<HuffmanNode>::iterator;

    FILE_SIZE = FileUtility::get_file_size(this->FILE_NAME);

    in.open(FILE_NAME, std::ios::in | std::ios::binary);
    if (!in.is_open()) {
        std::cerr << "Error in open file '" << FILE_NAME << "'" << std::endl;
        return "Error";
    }

    buffer = new char[FILE_SIZE];
    in.read(buffer, FILE_SIZE);
    for(int i = 0; i < FILE_SIZE; i++) {
        std::string rep = characterBitRepresentation.find(buffer[i])->second;
        fileContentHuffmanRepresentation.append(rep);
    }

    delete buffer;
    in.close();
    return fileContentHuffmanRepresentation;
}

char * HuffmanEncodingUtility::get_bytes_from_huffman_generated_data(std::string bitsRepresentation) {

    int             NUMBER_OF_BITS = bitsRepresentation.size();
    int             NUMBER_OF_BYTES;
    char *          data = NULL; //We choose to store all the bits in char [] type
    int             currentByteInDataArray = -1; // a variable to track in which byte 
                                                 //the data should be stored in the array
    unsigned int    bit;
    int             dataIndexInByte;

    //Get the number of BYTE needed to store the bits
    //knowing that we can only store a number of bit
    //multiple to 8
    if(NUMBER_OF_BITS % 8 == 0)
        NUMBER_OF_BYTES = NUMBER_OF_BITS / 8;
    else 
        NUMBER_OF_BYTES = (NUMBER_OF_BITS / 8) + 1;
    
    data = new char[NUMBER_OF_BYTES] {};

    for(int i = 0; i < bitsRepresentation.length(); i++) {
        
        bit = atoi(std::string(1, bitsRepresentation[i]).c_str()); //the bit value will be stored at 
                                                                   //the least significant bit
        dataIndexInByte = i % 8;
        if(dataIndexInByte == 0)
            currentByteInDataArray++;
        
        if(bit == 1)
            data[currentByteInDataArray] |= 1 << dataIndexInByte;
        else 
            data[currentByteInDataArray] &= ~(1 << dataIndexInByte);
    }
    return data;
}

std::string HuffmanEncodingUtility::get_huffman_generated_data_from_bytes(char *data, int NUMBER_OF_BITS) {

    std::string         bitsOfHuffmanGeneratedData;
    int                 NUMBER_OF_BYTES;
    int                 NUMBER_OF_SIGNFICANT_BITS;
    unsigned int        bit;
    char                bitChar;

    if(NUMBER_OF_BITS % 8 == 0)
        NUMBER_OF_BYTES = NUMBER_OF_BITS / 8;
    else 
        NUMBER_OF_BYTES = (NUMBER_OF_BITS / 8) + 1;
    for(int indexByte = 0; indexByte < NUMBER_OF_BYTES; indexByte++) {
        
        if(indexByte != NUMBER_OF_BYTES - 1) 
            NUMBER_OF_SIGNFICANT_BITS = 8;
        else
            NUMBER_OF_SIGNFICANT_BITS = (NUMBER_OF_BITS % 8 == 0) ? 8 : NUMBER_OF_BITS % 8;
        for(int indexBit = 0 ; indexBit < NUMBER_OF_SIGNFICANT_BITS; indexBit++) {
            bit = data[indexByte] & (1 << indexBit);
            bitChar;
            (bit != 0) ? bitChar = '1' : bitChar = '0';
            bitsOfHuffmanGeneratedData.push_back(bitChar);
        }
    }
    return bitsOfHuffmanGeneratedData;
}

std::string HuffmanEncodingUtility::restore_file_content_from_huffman_representation(std::string huffmanRep, HuffmanNode* huffmanRootNode) {
    
    HuffmanNode*            currentNode = huffmanRootNode;
    std::string             fileContent;
    char                    currentCharacter;

    for(int i = 0; i <= huffmanRep.size(); i++) {
        if(i != huffmanRep.size())
            currentCharacter = huffmanRep[i];
        if(currentNode->getNodeType() == NodeType::Normal) {
            fileContent += currentNode->getCharacter();
            currentNode = huffmanRootNode;
            i--;
        }
        else {
            if(currentCharacter == '0') {
                currentNode = currentNode->leftNode;
            }
            else {
                currentNode = currentNode->rightNode;
            }
        }   
    }
    return fileContent;
}

void HuffmanEncodingUtility::compress_data() {

    std::vector<HuffmanNode>        huffmanHeap;
    std::string                     fileContentInHuffmanFormat;
    char *                          bytesOfHuffmanRep;
    FILE *                          fp;
    int                             NUMBER_OF_BITS;
    int                             NUMBER_OF_BYTES;

    read_data_file(huffmanHeap);
    construct_huffman_tree(huffmanHeap);
    encode_huffman_tree(huffmanHeap[0]);

    fileContentInHuffmanFormat = get_file_content_in_huffman_format();
    bytesOfHuffmanRep = get_bytes_from_huffman_generated_data(fileContentInHuffmanFormat);
 
    fp = fopen((FILE_NAME + ".compressed").c_str(), "wb");
    HuffmanNode::serialize_huffman_tree(huffmanHeap[0], fp);

    NUMBER_OF_BITS = fileContentInHuffmanFormat.size();
    fwrite(&NUMBER_OF_BITS, sizeof(int), 1, fp);


    if(NUMBER_OF_BITS % 8 == 0)
        NUMBER_OF_BYTES = NUMBER_OF_BITS / 8;
    else 
        NUMBER_OF_BYTES = (NUMBER_OF_BITS / 8) + 1;

    fwrite(bytesOfHuffmanRep, sizeof(char), NUMBER_OF_BYTES, fp);
    fclose(fp);
}

void HuffmanEncodingUtility::uncompress_data() {

    FILE*                               fp;
    HuffmanNode*                        rootNode = NULL;
    int                                 NUMBER_OF_BITS;
    int                                 NUMBER_OF_BYTES;
    char *                              compressedBitsHuffman;
    FILE *                              fu;
    std::string                         huffmanBytesAsChars;
    std::string                         restoredData;

    fp = fopen((FILE_NAME).c_str(), "rb");

    if (fp == NULL) {
        std::cerr << "Error when opening file '" << FILE_NAME << "'" << std::endl;
        exit(1);
        return;
    }

    rootNode = NULL;
    HuffmanNode::deserialize_huffman_tree(rootNode, fp);

    
    fread(&NUMBER_OF_BITS, sizeof(int), 1, fp);
    
    if(NUMBER_OF_BITS % 8 == 0)
        NUMBER_OF_BYTES = NUMBER_OF_BITS / 8;
    else 
        NUMBER_OF_BYTES = (NUMBER_OF_BITS / 8) + 1;

    compressedBitsHuffman = (char*) malloc(NUMBER_OF_BYTES * sizeof(char));
    fread(compressedBitsHuffman, sizeof(char), NUMBER_OF_BYTES, fp);
    fclose(fp);

    fu = fopen((FILE_NAME + ".uncompressed.txt").c_str(), "wb");
    huffmanBytesAsChars = get_huffman_generated_data_from_bytes(compressedBitsHuffman, NUMBER_OF_BITS);
    restoredData = restore_file_content_from_huffman_representation(huffmanBytesAsChars, rootNode);

    fwrite(restoredData.c_str(), sizeof(char), restoredData.size(), fu);
    fclose(fu);
}
