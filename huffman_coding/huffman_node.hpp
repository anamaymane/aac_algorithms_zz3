/* -------------------------------------------------------------------- */
/* Nom du fichier: huffman_node.hpp                                     */
/* Rôle: Fichier d'entête, Définition d'une classe qui représente les   */
/*       noeuds de l'arbre de huffman générée à partir du fichier       */
/*       de données                                                     */
/* -------------------------------------------------------------------- */

enum class NodeType { Normal, Internal };

class HuffmanNode {
    private:
        char            character;          //Charactère du noeud
        int             frequency;          //Fréquence du caractère
        NodeType        nt;                 //Type du noeud
        std::string     representation;     //Représentation sous forme
                                            // de bits du noeud
        
    public:
        HuffmanNode*    leftNode;           //Noeud de droite pointé
        HuffmanNode*    rightNode;          //Noeud de gauche pointé

    public:

        /* -------------------------------------------------------------------- */
        /* HuffmanNode Constructeur par défaut de la classe HuffmanNode */
        /* */
        /* En entrée: Aucune entrée */
        /* -------------------------------------------------------------------- */
        HuffmanNode();
        
        /* -------------------------------------------------------------------- */
        /* HuffmanNode Constructeur par argument de la classe HuffmanNode      */
        /*                                                                      */
        /* En entrée: character : le caractère en qustion,                      */
        /*            frequency : sa fréquence d'apparition dans le fichier     */
        /*                        de données                                    */
        /*            nt: le type du noeud, est ce que c'est un noeud générée   */
        /*                ou contenant un caractère                             */
        /* -------------------------------------------------------------------- */
        HuffmanNode(char character, int frequency, NodeType nt = NodeType::Normal);

        /* -------------------------------------------------------------------- */
        /* HuffmanNode Constructeur de copie de la classe HuffmanNode */
        /* */
        /* En entrée: ef: un autre objet de la classe HuffmanNode */
        /* -------------------------------------------------------------------- */
        HuffmanNode(const HuffmanNode& ef);

        /* -------------------------------------------------------------------- */
        /* setCharacter setter du caractère */
        /* */
        /* En entrée: in : la valeur du caractère */
        /* -------------------------------------------------------------------- */
        void setCharacter(char in);

        /* -------------------------------------------------------------------- */
        /* setFrequency setter de la fréquence */
        /* */
        /* En entrée: freqeuncy: la fréquence du caractère */
        /* -------------------------------------------------------------------- */
        void setFrequency(int frequency);

        /* -------------------------------------------------------------------- */
        /* setBitRepresentation setter de la représentation sous forme de bit   */
        /*                      du noeud                                        */
        /* En entrée: bitRepresentation: fla représentation sous forme de string */
        /* -------------------------------------------------------------------- */
        void setBitRepresentation(std::string& bitRepresentation);

        /* -------------------------------------------------------------------- */
        /* setNodeType setter du node type */
        /* */
        /* En entrée: nt: le type du noeud */
        /* -------------------------------------------------------------------- */
        void setNodeType(NodeType nt);

        /* -------------------------------------------------------------------- */
        /* getCharacter getter de la valeur du caractère */
        /* */
        /* En entrée: aucune entrée */
        /* En sortie: la valeur du caractère */
        /* -------------------------------------------------------------------- */
        char getCharacter() const;

        /* -------------------------------------------------------------------- */
        /* getFrequency getter de la valeur de la fréquence */
        /* */
        /* En entrée: aucune entrée */
        /* En sortie: la valeur de la fréquence */
        /* -------------------------------------------------------------------- */
        int getFrequency() const;

        /* -------------------------------------------------------------------- */
        /* getNodeType getter du type du noeud */
        /* */
        /* En entrée: aucune entrée */
        /* En sortie: le type du noeud */
        /* -------------------------------------------------------------------- */
        NodeType getNodeType() const;

        /* -------------------------------------------------------------------- */
        /* getNodeBitRepresentation getter de la représentation du noeud */
        /* */
        /* En entrée: aucune entrée */
        /* En sortie: la représentation du noeud sous forme de bits */
        /* -------------------------------------------------------------------- */
        std::string getNodeBitRepresentation();

        /* --------------------------------------------------------------------  */
        /* serialize_huffman_tree méthode statique pour sérialiser sur disque    */
        /*                        l'arbre de huffman générée                     */
        /* En entrée: huffmanNode: la racine de l'arbre de huffman               */
        /*            fp : le pointeur du fichier sur lequel on écrit            */
        /* En sortie: aucune sortie                                              */
        /* --------------------------------------------------------------------  */
        static void serialize_huffman_tree(HuffmanNode& huffmanNode, FILE * fp);

        /* --------------------------------------------------------------------  */
        /* deserialize_huffman_tree méthode statique pour désérialiser du disque */
        /*                          l'arbre de huffman générée                   */
        /* En entrée: currentNode: un pointeur où stcoker la racine de l'arbre   */
        /*                         de huffman                                    */
        /*            fp : le pointeur du fichier à partir duquel on lit         */
        /* En sortie: aucune sortie                                              */
        /* --------------------------------------------------------------------  */
        static void deserialize_huffman_tree(HuffmanNode*& currentNode, FILE* fp);
};

/* --------------------------------------------------------------------  */
/* operator== Opérateur de comparaison d'égalité entre deux noeuds       */
/*             de Huffman                                                */
/* En entrée: ef1: le premier noeud                                      */
/*            ef2 : le deuxième noeud                                    */
/* En sortie: résultat booléen de la comparaison                         */
/* --------------------------------------------------------------------  */
bool operator==(const HuffmanNode& ef1, const HuffmanNode& ef2);

/* --------------------------------------------------------------------  */
/* operator== Opérateur de comparaison d'infériorité entre deux          */
/*            noeuds de Huffman                                          */
/* En entrée: ef1: le premier noeud                                      */
/*            ef2 : le deuxième noeud                                    */
/* En sortie: résultat booléen de la comparaison                         */
/* --------------------------------------------------------------------  */
bool operator<(const HuffmanNode& ef1, const HuffmanNode& ef2);