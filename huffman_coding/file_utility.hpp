/* -------------------------------------------------------------------- */
/* Nom du fichier: file_utility.hpp                                     */
/* Rôle: Fichier d'entête, Définition d'une classe qui permet de        */
/*       manipuler des fichiers                                         */
/* -------------------------------------------------------------------- */

#include <string.h>
#include <stdlib.h>
#include <fstream>
#include <string>

class FileUtility {
    public:

        /* -------------------------------------------------------------------- */
        /* get_file_size Méthode static pour avoir la taille d'un fichier       */
        /* En entrée: FILE_NAME: Le nom du fichier                              */
        /* En sortie: La taille du fichier en octects                           */
        /* -------------------------------------------------------------------- */
        static int get_file_size(std::string FILE_NAME);

        /* -------------------------------------------------------------------- */
        /* compareFiles Méthode pour comparer deux fichiers                     */
        /* En entrée: file1: pointeur sur le premier fichier                    */
        /*            file2: pointeur sur le deuxième fichier                   */
        /* En sortie: Résultat de la comparaison                                */
        /* -------------------------------------------------------------------- */
        static bool compareFiles(FILE *file1, FILE *file2);
};