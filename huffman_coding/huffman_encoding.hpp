/* -------------------------------------------------------------------- */
/* Nom du fichier: huffman_encoding.hpp                                 */
/* Rôle: Fichier d'entête, Définition d'une classe qui gère la          */
/*       compression et la décompression de fichiers via la méthode     */
/*       de huffman                                                     */
/* -------------------------------------------------------------------- */

#include <map>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cstring>

#include "huffman_node.hpp"

enum class OperationResult { Success, Failure };

class HuffmanEncodingUtility {

    public:

        std::string                         FILE_NAME;
        std::map<char, std::string>         characterBitRepresentation;

    public:

        /* -------------------------------------------------------------------- */
        /* read_data_file Construction de noeuds de huffman où chaque noeud     */
        /*                contient un caractère et ses informations             */
        /* En entrée: elements: un vecteur pour stocker les noeuds de huffman   */
        /*                      générés                                         */
        /* -------------------------------------------------------------------- */
        void read_data_file(std::vector<HuffmanNode>& elements);

        /* -------------------------------------------------------------------- */
        /* construct_huffman_tree Construction de l'arbre de huffman à partir   */
        /*                        de noeuds de type HuffmanNode                 */
        /* En entrée: huffmanHeap: un vecteur pour stocker les noeuds non encore*/
        /*                         traités par l'algorithme Huffman, à la fin   */
        /*                         la méthode, ce vecteur contiendera un seul   */
        /*                         noeud qui est la racine de l'arbre Huffman   */
        /* -------------------------------------------------------------------- */
        void construct_huffman_tree(std::vector<HuffmanNode>& huffmanHeap);

        /* -------------------------------------------------------------------- */
        /* encode_huffman_tree_recursive Associer le code Huffman à chaque noeud*/
        /*                        de l'arbre de Huffman                         */
        /* En entrée: node: la racine de l'arbre de huffamn                     */
        /*            rep: une chaine de caractère utilisé pour trouver r       */
        /*                 récursivement la représentation de chaque noeud      */
        /* -------------------------------------------------------------------- */
        void encode_huffman_tree_recursive(HuffmanNode& node, std::string& rep);

        /* -------------------------------------------------------------------- */
        /* encode_huffman_tree Appel la méthode récursive                       */
        /*                     encode_huffman_tree_recursive pour trouver la    */
        /*                     représentation des noeuds en bits                */
        /* En entrée: node: la racine de l'arbre de huffamn                     */
        /* -------------------------------------------------------------------- */
        void encode_huffman_tree(HuffmanNode& node);

        /* -------------------------------------------------------------------- */
        /* display_huffman_tree Méthode pour afficher l'arbre de huffman        */
        /* En entrée: node: la racine de l'arbre de huffamn                     */
        /* -------------------------------------------------------------------- */
        void display_huffman_tree(HuffmanNode& node);

        /* -------------------------------------------------------------------- */
        /* get_file_content_in_huffman_format Méthode pour avoir la             */
        /*                                    représentation du cotenu du       */
        /*                                    fichier sous forme de bits de     */
        /*                                    huffamn                           */
        /* En sortie: contenu du fichier sous format de bits de huffman stockés */
        /*            dans une chaîne de caractères                             */
        /* -------------------------------------------------------------------- */
        std::string get_file_content_in_huffman_format();

        /* -------------------------------------------------------------------- */
        /* get_bytes_from_huffman_generated_data Méthode pour générer des bits  */
        /*                                       réel à partir de la            */
        /*                                       représentasous sous forme de   */
        /*                                       caractères des bits            */
        /* En entrée: bitsRepresentation: représentation de bits sous forme de  */
        /*                                chaines de caractères                 */
        /* En sortie: bits réels de la représentation binaire de Huffman        */
        /* -------------------------------------------------------------------- */
        char * get_bytes_from_huffman_generated_data(std::string bitsRepresentation);

        /* -------------------------------------------------------------------- */
        /* get_huffman_generated_data_from_bytes Méthode pour générer une  */
        /*                                       représentation binaire sous    */
        /*                                       de chaînes de caractères à     */
        /*                                       partir d'un tableau de bytes   */
        /* En entrée: data: tableau de bytes contenant les données de huffman   */
        /*            NUMBER_OF_BITS: Nombre de bits rééllement stockées dans le*/
        /*            tableau de bytes                                          */
        /* En sortie: Bits de huffman sous forme de chaînes de caractères       */
        /* -------------------------------------------------------------------- */
        std::string get_huffman_generated_data_from_bytes(char *data, int NUMBER_OF_BITS);

        /* --------------------------------------------------------------------  */
        /* restore_file_content_from_huffman_representation Méthode pour restorer*/
        /*                                       le contenu textuel à partir     */
        /*                                       de la représentation sous       */
        /*                                       forme de bits de Huffman et     */
        /*                                       de l'arbre de Huffman           */
        /* En entrée: huffmanRep: représentation du contenu du fichier           */
        /*                        sous forme de bits de Huffman                  */
        /*            huffmanRootNode: Noeud root de l'arbre de Huffman          */
        /* En sortie: contenu textuel du fichier                                 */
        /* --------------------------------------------------------------------  */
        std::string restore_file_content_from_huffman_representation(std::string huffmanRep, HuffmanNode* huffmanRootNode);

        /* --------------------------------------------------------------------  */
        /* compress_data Méthode qui compresse le fichier stocké dans la variable*/
        /*                                       d'instance FILE_NAME,           */
        /*                                       le fichier généré aura comme    */
        /*                                       nom "FILE_NAME + .compressed"   */
        /* En sortie: le résultat de l'opération                                 */
        /* --------------------------------------------------------------------  */
        void compress_data();

        /* --------------------------------------------------------------------  */
        /* uncompress_data Méthode qui décompresse le fichier ayant comme path   */
        /*                                       la variable d'instance FILE_NAME*/
        /*                                       +  l'extension .compressed, le  */
        /*                                       fichier généré aura comme nom   */
        /*                                       FILE_NAME + ".uncompressed.txt" */
        /* En sortie: le résultat de l'opération                                 */
        /* --------------------------------------------------------------------  */
        void uncompress_data();
};
