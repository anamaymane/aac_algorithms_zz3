/* --------------------------------------------------------------------*/
/* Nom du fichier: huffman_node.cpp                                    */
/* Rôle: Fichier source, Implémentation des méthodes du fichier        */
/*       d'entête huffman_node.hpp                                     */
/* --------------------------------------------------------------------*/

#include <ostream>
#include "huffman_node.hpp"

HuffmanNode::HuffmanNode(){}

HuffmanNode::HuffmanNode(char character, int frequency, NodeType nt) {
    this->character = character;
    this->frequency = frequency;
    this->nt = nt;
    this->leftNode = NULL;
    this->rightNode = NULL;
}

HuffmanNode::HuffmanNode(const HuffmanNode& ef) {
    this->character = ef.getCharacter();    
    this->frequency = ef.getFrequency();
    this->nt = ef.getNodeType();
    this->leftNode = ef.leftNode;
    this->rightNode = ef.rightNode;
}

void HuffmanNode::setCharacter(char in) {
            this->character = in;
}

void HuffmanNode::setFrequency(int frequency) {
    this->frequency = frequency;
}

void HuffmanNode::setBitRepresentation(std::string& bitRepresentation) {
    this->representation = bitRepresentation;
}

void HuffmanNode::setNodeType(NodeType nt) {
    this->nt = nt;
}

char HuffmanNode::getCharacter() const {
    return this->character;
}

int HuffmanNode::getFrequency() const {
    return this->frequency;
}


NodeType HuffmanNode::getNodeType() const {
    return this->nt;
}

std::string HuffmanNode::getNodeBitRepresentation() {
    return this->representation;
}

void HuffmanNode::serialize_huffman_tree(HuffmanNode& huffmanNode, FILE * fp) {
    fwrite(&huffmanNode, sizeof(HuffmanNode), 1, fp);
    if(huffmanNode.leftNode != NULL)
        serialize_huffman_tree(*huffmanNode.leftNode, fp);
    if(huffmanNode.rightNode != NULL) 
        serialize_huffman_tree(*huffmanNode.rightNode, fp);
}

void HuffmanNode::deserialize_huffman_tree(HuffmanNode*& currentNode, FILE* fp) {
    HuffmanNode *       tempNode;

    if(currentNode == NULL) {
        currentNode = new HuffmanNode();
        fread(currentNode, sizeof(HuffmanNode), 1, fp);
    }
    if(currentNode->getNodeType() == NodeType::Normal)
        return;
    else {
        tempNode = new HuffmanNode();
        fread(tempNode, sizeof(HuffmanNode), 1, fp);
        currentNode->leftNode = tempNode;
        deserialize_huffman_tree(currentNode->leftNode, fp);
        tempNode = new HuffmanNode();
        fread(tempNode, sizeof(HuffmanNode), 1, fp);
        currentNode->rightNode = tempNode;
        deserialize_huffman_tree(currentNode->rightNode, fp);
        return;
    }
}

bool operator==(const HuffmanNode& ef1, const HuffmanNode& ef2) {
    return ef1.getCharacter() == ef2.getCharacter();
}

std::ostream & operator<<(std::ostream& os, const HuffmanNode& ef1) {
    os << ef1.getCharacter();
    return os;
}

bool operator<(const HuffmanNode& ef1, const HuffmanNode& ef2) {
    return ef1.getFrequency() < ef2.getFrequency() && ef1.getCharacter() != ef2.getCharacter();
}