/* -------------------------------------------------------------------- */
/* Nom du fichier: main_test.cpp                                        */
/* Rôle: Programme principale pour faire tourner les tests unitaires    */
/*       écrits avec la bibliothèque catch, le exe de ce programme      */
/*       principale de tests peut être généré avec la commande make test*/
/* -------------------------------------------------------------------- */

#define CATCH_CONFIG_MAIN
#include "library/catch.hpp"
